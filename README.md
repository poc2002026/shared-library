# shared-library

This is a simple PoC repository for hosting shared jobs.
Currently it has 3 types of jobs:
- Gitleaks for secret detection 
- OWASP dependency check for SCA
- Semgrep for SAST
